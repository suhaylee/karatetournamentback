-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 05 2017 г., 11:17
-- Версия сервера: 10.1.21-MariaDB
-- Версия PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `karate`
--

-- --------------------------------------------------------

--
-- Структура таблицы `club`
--

CREATE TABLE `club` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `club`
--

INSERT INTO `club` (`id`, `name`) VALUES
(1, 'Real Madrid'),
(2, 'Barcelona');

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `tour` int(11) NOT NULL,
  `participant1` int(11) NOT NULL,
  `participant2` int(11) DEFAULT NULL,
  `winner` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `games`
--

INSERT INTO `games` (`id`, `tournament_id`, `tour`, `participant1`, `participant2`, `winner`, `comment`) VALUES
(7, 1, 1, 9, 12, 1, NULL),
(8, 1, 1, 10, 13, 1, NULL),
(9, 1, 1, 25, NULL, 1, NULL),
(10, 1, 1, 23, 1, 1, NULL),
(11, 1, 1, 22, 24, 1, NULL),
(14, 1, 2, 10, 9, 2, 'asdasd'),
(15, 1, 2, 25, NULL, 1, ''),
(16, 1, 3, 9, 25, 1, NULL),
(26, 3, 1, 33, 37, 1, NULL),
(27, 3, 1, 35, 39, 2, NULL),
(28, 3, 1, 41, 43, 3, NULL),
(29, 3, 1, 45, NULL, 1, NULL),
(30, 3, 1, 28, 40, 2, NULL),
(31, 3, 1, 32, 38, 2, NULL),
(32, 3, 1, 34, 42, 1, NULL),
(33, 3, 1, 36, 44, 2, NULL),
(34, 3, 1, 46, NULL, 1, NULL),
(40, 3, 2, 45, 33, 2, NULL),
(41, 3, 2, 39, NULL, 1, NULL),
(42, 3, 2, 46, 34, 1, NULL),
(43, 3, 2, 44, 38, 1, NULL),
(44, 3, 2, 40, NULL, 1, NULL),
(45, 3, 3, 39, 33, 1, NULL),
(46, 3, 3, 40, 44, 2, NULL),
(47, 3, 3, 46, NULL, 1, NULL),
(48, 3, 4, 46, 44, 1, NULL),
(49, 8, 1, 58, 64, 1, NULL),
(50, 8, 1, 60, 62, 2, NULL),
(51, 8, 1, 59, 65, 1, NULL),
(52, 8, 1, 63, 61, 1, NULL),
(53, 9, 1, 68, 80, 2, NULL),
(54, 9, 1, 70, 78, 2, NULL),
(55, 9, 1, 72, 84, 1, NULL),
(56, 9, 1, 74, 82, 3, NULL),
(57, 9, 1, 76, 86, 2, NULL),
(58, 9, 1, 69, 81, 1, NULL),
(59, 9, 1, 71, 79, 2, NULL),
(60, 9, 1, 73, 85, 1, NULL),
(61, 9, 1, 75, 83, 2, NULL),
(62, 9, 1, 77, 87, 1, NULL),
(63, 9, 2, 86, 72, 1, NULL),
(64, 9, 2, 78, 80, 2, NULL),
(65, 9, 2, 77, 83, 1, NULL),
(66, 9, 2, 73, 79, 2, NULL),
(67, 9, 2, 69, NULL, 1, NULL),
(68, 9, 3, 80, 86, 1, NULL),
(69, 9, 3, 69, 79, 1, NULL),
(70, 9, 3, 77, NULL, 1, NULL),
(71, 9, 4, 77, 69, 1, NULL),
(72, 7, 1, 47, 48, 2, NULL),
(73, 7, 1, 50, 52, 1, NULL),
(74, 7, 1, 54, 56, 1, NULL),
(75, 7, 1, 49, 51, 2, NULL),
(76, 7, 1, 53, 55, 2, NULL),
(77, 7, 1, 57, NULL, 1, NULL),
(78, 7, 2, 54, 50, 2, NULL),
(79, 7, 2, 48, NULL, 1, NULL),
(80, 7, 2, 57, 55, 2, NULL),
(81, 7, 2, 51, NULL, 1, NULL),
(84, 7, 3, 48, 50, 1, NULL),
(85, 7, 3, 51, 55, 2, NULL),
(86, 10, 1, 89, 97, 1, NULL),
(87, 10, 1, 99, 111, 2, NULL),
(88, 10, 1, 101, 109, 1, NULL),
(89, 10, 1, 103, 115, 2, NULL),
(90, 10, 1, 105, 113, 1, NULL),
(91, 10, 1, 107, 117, 2, NULL),
(92, 10, 1, 88, 110, 2, NULL),
(93, 10, 1, 90, 112, 1, NULL),
(94, 10, 1, 96, 114, 1, NULL),
(95, 10, 1, 102, 116, 2, NULL),
(96, 10, 1, 104, 118, 1, NULL),
(97, 10, 1, 106, 108, 2, NULL),
(98, 10, 2, 117, 105, 2, NULL),
(99, 10, 2, 115, 101, 2, NULL),
(100, 10, 2, 111, 89, 1, NULL),
(101, 10, 2, 108, 116, 2, NULL),
(102, 10, 2, 104, 110, 1, NULL),
(103, 10, 2, 96, 90, 2, NULL),
(104, 10, 3, 111, 105, 1, NULL),
(105, 10, 3, 101, NULL, 1, NULL),
(106, 10, 3, 90, 116, 2, NULL),
(107, 10, 3, 104, NULL, 1, NULL),
(108, 10, 4, 101, 111, 1, NULL),
(109, 10, 4, 104, 116, 2, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `participant`
--

CREATE TABLE `participant` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `surname` varchar(50) COLLATE utf8_bin NOT NULL,
  `patronymic` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `weight` double NOT NULL,
  `age` int(11) NOT NULL,
  `city` varchar(50) COLLATE utf8_bin NOT NULL,
  `country` varchar(50) COLLATE utf8_bin NOT NULL,
  `sex` varchar(50) COLLATE utf8_bin NOT NULL,
  `rating` int(11) NOT NULL,
  `photo` varchar(80) COLLATE utf8_bin DEFAULT NULL,
  `club_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `participant`
--

INSERT INTO `participant` (`id`, `name`, `surname`, `patronymic`, `weight`, `age`, `city`, `country`, `sex`, `rating`, `photo`, `club_id`, `tournament_id`) VALUES
(1, 'Сухайли', 'Баходури', 'Tillavzod', 20, 20, 'Алматы', 'Казахстан', 'Мужской', 9, 'sdf', 1, 1),
(2, 'Сухайли', 'Баходури', 'Tillavzod', 63, 20, 'Алматы', 'Казахстан', 'Мужской', 9, 'sdf', 1, 1),
(3, 'dfg', 'dfgdfg', 'dfg', 60, 23, 'AVC', 'dfg', 'female', 3, 'basicPhoto.jpg', 2, 1),
(6, 'wesff', 'sadff', 'sadf', 30.4, 2, 'dfgdfg', 'dfgdfg', 'Женский', 3, 'basicPhoto.jpg', 1, 1),
(9, 'Jason', 'Lareu', 'Static', 12, 12, 'Dushanbe', 'USA', 'Мужской', 1, 'basicPhoto.jpg', 1, 1),
(10, 'Jason', 'Lareu', 'Static', 12, 12, 'Dushanbe', 'USA', 'Мужской', 1, 'basicPhoto.jpg', 2, 1),
(12, 'Jason1', 'Lareu', 'Static', 13, 14, 'New York', 'USA', 'Мужской', 2, 'basicPhoto.jpg', 2, 1),
(13, 'Jason2', 'Lareu', 'Static', 13, 16, 'Almaty', 'USA', 'Мужской', 3, 'basicPhoto.jpg', 2, 1),
(16, 'Jason5', 'Lareu', 'Static', 17, 22, 'New York', 'USA', 'Женский', 6, 'basicPhoto.jpg', 2, 1),
(21, 'Jason', 'Lareu', 'Static', 17, 12, 'Dushanbe', 'USA', 'Мужской', 1, 'basicPhoto.jpg', 2, 1),
(22, 'Jason1', 'Lareu', 'Static', 18, 14, 'New York', 'USA', 'Женский', 2, 'basicPhoto.jpg', 2, 1),
(23, 'Jason2', 'Lareu', 'Static', 18, 16, 'Almaty', 'USA', 'Мужской', 3, 'basicPhoto.jpg', 2, 1),
(24, 'Jason3', 'Lareu', 'Static', 19, 18, 'Antalya', 'USA', 'Женский', 4, 'basicPhoto.jpg', 2, 1),
(25, 'Jason4', 'Lareu', 'Static', 14, 20, 'Dushanbe', 'USA', 'Мужской', 5, 'basicPhoto.jpg', 2, 1),
(26, 'Jason5', 'Lareu', 'Static', 14, 22, 'New York', 'USA', 'Женский', 6, 'basicPhoto.jpg', 2, 1),
(27, 'Jason', 'Lareu', 'Static', 55, 12, 'Dushanbe', 'USA', '???????', 1, 'basicPhoto.jpg', 2, 3),
(28, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 2, 3),
(29, 'Jason2', 'Lareu', 'Static', 65, 16, 'Almaty', 'USA', '???????', 3, 'basicPhoto.jpg', 2, 3),
(30, 'Jason3', 'Lareu', 'Static', 55, 18, 'Antalya', 'USA', '???????', 4, 'basicPhoto.jpg', 1, 3),
(32, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 2, 3),
(33, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 2, 3),
(34, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 2, 3),
(35, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 2, 3),
(36, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 2, 3),
(37, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'male', 1, 'basicPhoto.jpg', 1, 3),
(38, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 1, 3),
(39, 'Jason2', 'Lareu', 'Static', 0, 16, 'Almaty', 'USA', 'male', 3, 'basicPhoto.jpg', 1, 3),
(40, 'Jason3', 'Lareu', 'Static', 0, 18, 'Antalya', 'USA', 'female', 4, 'basicPhoto.jpg', 1, 3),
(41, 'Jason4', 'Lareu', 'Static', 0, 20, 'Dushanbe', 'USA', 'male', 5, 'basicPhoto.jpg', 1, 3),
(42, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 1, 3),
(43, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 1, 3),
(44, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 1, 3),
(45, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 1, 3),
(46, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 1, 3),
(47, 'erret', 'ert', 'ert', 0, 34, 'dfgdfg', 'dfgdfg', 'Мужской', 3, 'basicPhoto.jpg', 1, 7),
(48, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'male', 1, 'basicPhoto.jpg', 1, 7),
(49, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 1, 7),
(50, 'Jason2', 'Lareu', 'Static', 0, 16, 'Almaty', 'USA', 'male', 3, 'basicPhoto.jpg', 1, 7),
(51, 'Jason3', 'Lareu', 'Static', 0, 18, 'Antalya', 'USA', 'female', 4, 'basicPhoto.jpg', 1, 7),
(52, 'Jason4', 'Lareu', 'Static', 0, 20, 'Dushanbe', 'USA', 'male', 5, 'basicPhoto.jpg', 1, 7),
(53, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 1, 7),
(54, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 1, 7),
(55, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 1, 7),
(56, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 1, 7),
(57, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 1, 7),
(58, 'Jason', 'Lareu', 'Static', 12, 12, 'Dushanbe', 'USA', 'Мужской', 1, 'basicPhoto.jpg', 1, 8),
(59, 'Jason1', 'Lareu', 'Static', 13, 14, 'New York', 'USA', 'Женский', 2, 'basicPhoto.jpg', 1, 8),
(60, 'Jason2', 'Lareu', 'Static', 34, 16, 'Almaty', 'USA', 'Мужской', 3, 'basicPhoto.jpg', 1, 8),
(61, 'Jason3', 'Lareu', 'Static', 35, 18, 'Antalya', 'USA', 'Женский', 4, 'basicPhoto.jpg', 1, 8),
(62, 'Jason4', 'Lareu', 'Static', 35, 20, 'Dushanbe', 'USA', 'Мужской', 5, 'basicPhoto.jpg', 1, 8),
(63, 'Jason5', 'Lareu', 'Static', 33, 22, 'New York', 'USA', 'Женский', 6, 'basicPhoto.jpg', 1, 8),
(64, 'Jason6', 'Lareu', 'Static', 13, 24, 'Almaty', 'USA', 'Мужской', 7, 'basicPhoto.jpg', 1, 8),
(65, 'Jason7', 'Lareu', 'Static', 14, 26, 'Antalya', 'USA', 'Женский', 8, 'basicPhoto.jpg', 1, 8),
(66, 'Jason8', 'Lareu', 'Static', 10, 28, 'Astana', 'USA', 'Мужской', 9, 'basicPhoto.jpg', 1, 8),
(67, 'Jason9', 'Lareu', 'Static', 11, 30, 'New York', 'USA', 'Женский', 10, 'basicPhoto.jpg', 1, 8),
(68, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'male', 1, 'basicPhoto.jpg', 1, 9),
(69, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 1, 9),
(70, 'Jason2', 'Lareu', 'Static', 0, 16, 'Almaty', 'USA', 'male', 3, 'basicPhoto.jpg', 1, 9),
(71, 'Jason3', 'Lareu', 'Static', 0, 18, 'Antalya', 'USA', 'female', 4, 'basicPhoto.jpg', 1, 9),
(72, 'Jason4', 'Lareu', 'Static', 0, 20, 'Dushanbe', 'USA', 'male', 5, 'basicPhoto.jpg', 1, 9),
(73, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 1, 9),
(74, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 1, 9),
(75, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 1, 9),
(76, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 1, 9),
(77, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 1, 9),
(78, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'male', 1, 'basicPhoto.jpg', 2, 9),
(79, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 2, 9),
(80, 'Jason2', 'Lareu', 'Static', 0, 16, 'Almaty', 'USA', 'male', 3, 'basicPhoto.jpg', 2, 9),
(81, 'Jason3', 'Lareu', 'Static', 0, 18, 'Antalya', 'USA', 'female', 4, 'basicPhoto.jpg', 2, 9),
(82, 'Jason4', 'Lareu', 'Static', 0, 20, 'Dushanbe', 'USA', 'male', 5, 'basicPhoto.jpg', 2, 9),
(83, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 2, 9),
(84, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 2, 9),
(85, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 2, 9),
(86, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 2, 9),
(87, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 2, 9),
(88, 'aesf', 'sdaf', 'sdaf', 0, 2, 'sdfg', 'sdg', 'Женский', 234, 'basicPhoto.jpg', 1, 10),
(89, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'Мужской', 1, 'basicPhoto.jpg', 2, 10),
(90, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 1, 10),
(96, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 1, 10),
(97, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 1, 10),
(99, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'male', 1, 'basicPhoto.jpg', 1, 10),
(101, 'Jason2', 'Lareu', 'Static', 0, 16, 'Almaty', 'USA', 'male', 3, 'basicPhoto.jpg', 1, 10),
(102, 'Jason3', 'Lareu', 'Static', 0, 18, 'Antalya', 'USA', 'female', 4, 'basicPhoto.jpg', 1, 10),
(103, 'Jason4', 'Lareu', 'Static', 0, 20, 'Dushanbe', 'USA', 'male', 5, 'basicPhoto.jpg', 1, 10),
(104, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 1, 10),
(105, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 1, 10),
(106, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 1, 10),
(107, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 1, 10),
(108, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 1, 10),
(109, 'Jason', 'Lareu', 'Static', 0, 12, 'Dushanbe', 'USA', 'male', 1, 'basicPhoto.jpg', 2, 10),
(110, 'Jason1', 'Lareu', 'Static', 0, 14, 'New York', 'USA', 'female', 2, 'basicPhoto.jpg', 2, 10),
(111, 'Jason2', 'Lareu', 'Static', 0, 16, 'Almaty', 'USA', 'male', 3, 'basicPhoto.jpg', 2, 10),
(112, 'Jason3', 'Lareu', 'Static', 0, 18, 'Antalya', 'USA', 'female', 4, 'basicPhoto.jpg', 2, 10),
(113, 'Jason4', 'Lareu', 'Static', 0, 20, 'Dushanbe', 'USA', 'male', 5, 'basicPhoto.jpg', 2, 10),
(114, 'Jason5', 'Lareu', 'Static', 0, 22, 'New York', 'USA', 'female', 6, 'basicPhoto.jpg', 2, 10),
(115, 'Jason6', 'Lareu', 'Static', 0, 24, 'Almaty', 'USA', 'male', 7, 'basicPhoto.jpg', 2, 10),
(116, 'Jason7', 'Lareu', 'Static', 0, 26, 'Antalya', 'USA', 'female', 8, 'basicPhoto.jpg', 2, 10),
(117, 'Jason8', 'Lareu', 'Static', 0, 28, 'Astana', 'USA', 'male', 9, 'basicPhoto.jpg', 2, 10),
(118, 'Jason9', 'Lareu', 'Static', 0, 30, 'New York', 'USA', 'female', 10, 'basicPhoto.jpg', 2, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `tournament`
--

CREATE TABLE `tournament` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `status` varchar(50) COLLATE utf8_bin NOT NULL,
  `startDate` bigint(20) NOT NULL,
  `endDate` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `tournament`
--

INSERT INTO `tournament` (`id`, `name`, `status`, `startDate`, `endDate`) VALUES
(1, 'First tournament', 'archived', 0, 0),
(3, 'Karate tournament', 'finished', 0, 0),
(4, 'saasd', 'started', 0, 0),
(7, 'ыаолдвыаод', 'FINISHED', 0, 0),
(8, 'Проверка 1', 'FINISHED', 0, 0),
(9, 'skldjfkdjf', 'FINISHED', 0, 0),
(10, 'Last test', 'archived', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(250) COLLATE utf8_bin NOT NULL,
  `role` varchar(50) COLLATE utf8_bin NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `surname` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `role`, `name`, `surname`) VALUES
(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ADMIN', 'admin', 'admin'),
(4, 'judge', '10e86c6514d40f2a3e861b31847340ee8c8ed181029a17b042f137121d28863e', 'JUDGE', 'Судья', 'judge');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participant1` (`participant1`),
  ADD KEY `participant2` (`participant2`),
  ADD KEY `tournament_id` (`tournament_id`);

--
-- Индексы таблицы `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_id` (`club_id`),
  ADD KEY `tournament_id` (`tournament_id`);

--
-- Индексы таблицы `tournament`
--
ALTER TABLE `tournament`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `club`
--
ALTER TABLE `club`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT для таблицы `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT для таблицы `tournament`
--
ALTER TABLE `tournament`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `games_ibfk_1` FOREIGN KEY (`participant1`) REFERENCES `participant` (`id`),
  ADD CONSTRAINT `games_ibfk_2` FOREIGN KEY (`participant2`) REFERENCES `participant` (`id`),
  ADD CONSTRAINT `games_ibfk_3` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`);

--
-- Ограничения внешнего ключа таблицы `participant`
--
ALTER TABLE `participant`
  ADD CONSTRAINT `participant_ibfk_3` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `participant_ibfk_4` FOREIGN KEY (`tournament_id`) REFERENCES `tournament` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
