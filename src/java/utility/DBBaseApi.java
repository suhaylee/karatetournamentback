package utility;

import entity.Tournament;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.QueryException;
import org.hibernate.criterion.Restrictions;
import static utility.DBConnect.session;

public class DBBaseApi extends DBConnect {

    public void delete(Object obj) {
        try {
            session().delete(obj);
            session().flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }

    public Object setObject(Object obj) {
        try {
            obj = session().merge(obj);
            session().flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        return obj;
    }

    public List getAllByType(Class type) {
        List result = null;
        try {
            result = session().createCriteria(type).list();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(result.isEmpty()){
            return null;
        } else{
            return (ArrayList)result;
        }
    }
    
    public List getAllParticipantsByTournamentId(Integer tournamentId) {
        List result = null;
        try {
            //result = session().createCriteria(type).list();
            Query query = session().createQuery("from Participant where tournament_id = :tournamentId ");
            query.setParameter("tournamentId", tournamentId);
            result = query.list();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(result.isEmpty()){
            return null;
        } else{
            return (ArrayList)result;
        }
    }
    
    public List getAllGamesByTournamentId(Integer tournamentId) {
        List result = null;
        try {
            //result = session().createCriteria(type).list();
            Query query = session().createQuery("from Games where tournament_id = :tournamentId order by tour");
            query.setParameter("tournamentId", tournamentId);
            result = query.list();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        if(result.isEmpty()){
            return null;
        } else{
            return (ArrayList)result;
        }
    }
    
    public User findByLoginAndPwd(String login, String pwd) {
        User user = (User) session().createCriteria(User.class)
                .add(Restrictions.eq("login", login))
                .add(Restrictions.eq("password", pwd))
                .uniqueResult();

        return user;
    }

    public User findByLogin(String login) {
        User user = (User) session().createCriteria(User.class)
                .add(Restrictions.eq("login", login))
                .uniqueResult();

        return user;
    }
    
//    public Object getObjectByTypeAndId(Class type, Long id) {
//        try {
//            Object result = session().get(type, id);
//            return result;
//        } catch (HibernateException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
    
    public Object getObjectByTypeAndId(Class type, Integer id) {
        try {
            Object result = session().get(type, id);
            return result;
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Object getObjectByTypeAndCode(Class type, String code) {
        try {
            Query query = session().createQuery("from " + type.getSimpleName() + " where upper(code)=upper(:code)");
            query.setParameter("code", code);
            return query.uniqueResult();
        } catch (QueryException ex) {
            ex.printStackTrace();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void refresh(Object object) {
        session().refresh(object);
    }

}
