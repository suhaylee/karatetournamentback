package utility;

import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Bagdat
 */
public class DBConnect {

    private static final SessionFactory sessionFactory;

    static {
        try {
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    private static final ThreadLocal<Session> localSessions = new ThreadLocal<Session>() {
        protected Object initialvalue() {
            return null;
        }
    };

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static Session session() {
        Session session = localSessions.get();
        if (session == null) {
            throw new RuntimeException("No session");
        }
        return session;
    }

    public static void init() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        localSessions.set(session);
    }

    public static void destroy() {
        if (session().isOpen()) {
            Transaction transaction = session().getTransaction();
            if (transaction.isActive()) {
                transaction.commit();
            }
            session().close();
        }
    }
}
