/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import entity.User;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import utility.DBBaseApi;

/**
 *
 * @author Админ
 */
public class UserHelper extends DBBaseApi {

    public UserHelper() {
    }

    public void saveDataUser(String login, String password, String role, String name, String surname) {

        Session session = null;
        Transaction txn = null;
        List<User> list = null;
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            session = sessionFactory.openSession();

            User user = new User();

            if (login != "") {
                user.setLogin(login);
                user.setPassword(password);
                user.setName(name);
                user.setSurname(surname);
                user.setRole(role);
            }

            User us = new User("2", "2", "2", "2", "2");
            txn = session.beginTransaction();
            session.save(us);
            session.save(user);
            txn.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (!txn.wasCommitted()) {
                txn.rollback();
            }
            session.flush();
            session.close();
        }
    }

    public List<User> userList() {
        List<User> list = null;

        Configuration cfg = new Configuration();
        SessionFactory factory = cfg.configure().buildSessionFactory();
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Query q = session.createQuery("from User");
        tx.commit();
        list = q.list();
        return list;
    }

    public User findByLoginAndPwd(String login, String pwd) {
        User user = (User) session().createCriteria(User.class)
                .add(Restrictions.eq("login", login))
                .add(Restrictions.eq("password", pwd))
                .uniqueResult();

        return user;
    }

    public User findByLogin(String login) {
        User user = (User) session().createCriteria(User.class)
                .add(Restrictions.eq("login", login))
                .uniqueResult();

        return user;
    }

}
