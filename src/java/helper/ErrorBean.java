package helper;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Админ
 */
public class ErrorBean implements Serializable {

    private String code;
    private String message;

    public ErrorBean(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
