package entity;
// Generated May 4, 2017 2:33:33 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Tournament generated by hbm2java
 */
public class Tournament  implements java.io.Serializable {


     private Integer id;
     private String name;
     private String status;
     private long startDate;
     private long endDate;
     private Set participants = new HashSet(0);
     private Set gameses = new HashSet(0);

    public Tournament() {
    }

	
    public Tournament(String name, String status, long startDate, long endDate) {
        this.name = name;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    public Tournament(String name, String status, long startDate, long endDate, Set participants, Set gameses) {
       this.name = name;
       this.status = status;
       this.startDate = startDate;
       this.endDate = endDate;
       this.participants = participants;
       this.gameses = gameses;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public long getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }
    public long getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
    public Set getParticipants() {
        return this.participants;
    }
    
    public void setParticipants(Set participants) {
        this.participants = participants;
    }
    public Set getGameses() {
        return this.gameses;
    }
    
    public void setGameses(Set gameses) {
        this.gameses = gameses;
    }




}


