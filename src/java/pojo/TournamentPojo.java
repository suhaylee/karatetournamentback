package pojo;

import entity.Tournament;

/**
 *
 * @author Админ
 */
public class TournamentPojo {

    private Integer id;
    private String name;
    private String status;

    public TournamentPojo() {

    }

    public TournamentPojo(Tournament tournament) {
        this.id = tournament.getId();
        this.name = tournament.getName();
        this.status = tournament.getStatus();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
