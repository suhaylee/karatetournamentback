/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Club;
import entity.Tournament;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pojo.ClubPojo;
import pojo.TournamentPojo;
import utility.DBApi;

/**
 * REST Web Service
 *
 * @author Админ
 */
@Path("club")
public class ClubResource {

    private DBApi dbApi = new DBApi();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        try {
            DBApi.init();
            List<Club> list = dbApi.getAllByType(Club.class);
            if (list != null && !list.isEmpty()) {
                ClubPojo[] club = new ClubPojo[list.size()];
                for (int i = 0; i < club.length; i++) {
                    club[i] = new ClubPojo(list.get(i));
                }
                return Response.ok(club).build();
            }
            return Response.ok(new ClubPojo[0]).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        try {
            System.out.println(id);
            DBApi.init();
            Club club = (Club) dbApi.getObjectByTypeAndId(Club.class, id);
            if (club != null) {
                dbApi.delete(club);
            }
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Club club) {
        try {
            System.out.println(club);
            DBApi.init();
            if (club != null) {
                dbApi.setObject(club);
                return Response.ok().build();
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, Club clubNew) {
        try {
            System.out.println(clubNew);
            DBApi.init();
            if (id != null) {
                Club club = (Club) dbApi.getObjectByTypeAndId(Club.class, id);
                if (club != null && clubNew != null) {
                    club.setName(clubNew.getName());
                    dbApi.setObject(club);
                    return Response.ok().build();
                }
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
}
