/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Participant;
import entity.Club;
import entity.Tournament;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pojo.ClubPojo;
import pojo.DataArray;
import pojo.ParticipantPojo;
import utility.DBApi;

/**
 * REST Web Service
 *
 * @author Админ
 */
@Path("participant")
public class ParticipantResource {

    private DBApi dbApi = new DBApi();

    @GET
    @Path("/{tournamentId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@PathParam("tournamentId") Integer tournamentId) {
        try {
            DBApi.init();
            
            System.out.println(tournamentId);
            
            List<Participant> list = dbApi.getAllParticipantsByTournamentId(tournamentId);
            if (list != null && !list.isEmpty()) {
                ParticipantPojo[] participant = new ParticipantPojo[list.size()];
                for (int i = 0; i < participant.length; i++) {
                    participant[i] = new ParticipantPojo(list.get(i));
                    System.out.println(participant[i].getSex().toLowerCase());
                    if(participant[i].getSex().toLowerCase().equals("male")){
                        participant[i].setSex("Мужской");
                    } else if(participant[i].getSex().toLowerCase().equals("female")){
                        participant[i].setSex("Женский");
                    }
                }
                return Response.ok(participant).build();
            }
            return Response.ok(new ParticipantPojo[0]).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        try {
            System.out.println(id);
            DBApi.init();
            Participant participant = (Participant) dbApi.getObjectByTypeAndId(Participant.class, id);
            if (participant != null) {
                dbApi.delete(participant);
            }
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @POST
    @Path("/{tournamentId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(@PathParam("tournamentId") Integer tournamentId, DataArray participants) {
        try {
            DBApi.init();
            if(tournamentId != null){
                for(int i=0;i<participants.data.length;i++){
                    ParticipantPojo participant = participants.data[i];
                    if (participant != null) {
                        Participant participantNew = new Participant(participant);
                        Tournament tournament = (Tournament) dbApi.getObjectByTypeAndId(Tournament.class, tournamentId);
                        if(tournament != null && tournament.getStatus().toLowerCase().equals("новый") || tournament.getStatus().toLowerCase().equals("new")){
                        participantNew.setTournament(tournament);
                            Club club = (Club) dbApi.getObjectByTypeAndId(Club.class, Integer.parseInt(participant.getClub()));
                            participantNew.setClub(club);
                            if(participantNew.getPhoto() == null){
                                participantNew.setPhoto("basicPhoto.jpg");
                            }
                            if(participantNew.getPatronymic()== null){
                                participantNew.setPatronymic("&nbsp");
                            }
                            if(participantNew.getSex().toLowerCase().equals("Мужской")){
                                participantNew.setSex("male");
                            } else if(participantNew.getSex().toLowerCase().equals("Женский")){
                                participantNew.setSex("female");
                            }
                            dbApi.setObject(participantNew);
                        }
                    }
                }
                return Response.ok().build();
            } 
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, ParticipantPojo participantNew) {
        try {
            DBApi.init();
            if (id != null) {
                Participant participant = (Participant) dbApi.getObjectByTypeAndId(Participant.class, id);
                if (participant != null && participantNew != null) {
                    Tournament tournament = participant.getTournament();
                    System.out.println("Tournament status: " + tournament.getStatus());
                    if(tournament != null && tournament.getStatus().toLowerCase().equals("новый") || tournament.getStatus().toLowerCase().equals("new")){
                        participant.setAll(participantNew);
                        Club club = (Club) dbApi.getObjectByTypeAndId(Club.class, Integer.parseInt(participantNew.getClub()));
                        participant.setClub(club);

                        if(participant.getPhoto() == null){
                            participant.setPhoto("basicPhoto.jpg");
                        }
                        if(participant.getPatronymic()== null){
                            participant.setPatronymic("&nbsp");
                        }

                        dbApi.setObject(participant);
                        return Response.ok().build();
                    }
                }
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
}
