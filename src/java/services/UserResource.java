/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Club;
import entity.User;
import helper.UserHelper;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pojo.AuthData;
import pojo.ClubPojo;
import pojo.UserPojo;
import utility.DBApi;

/**
 * REST Web Service
 *
 * @author Админ
 */
@Path("user")
public class UserResource {

    private UserHelper helper;
    private DBApi dbApi = new DBApi();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(){
        try {
            DBApi.init();
            List<User> list = dbApi.getAllByType(User.class);
            if (list != null && !list.isEmpty()) {
                User[] users = new User[list.size()];
                for (int i = 0; i < users.length; i++) {
                    users[i] = new User(list.get(i));
                }
                return Response.ok(users).build();
            }
            return Response.ok(new User[0]).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
    
    @POST
    @Path("authorize")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authorize(final AuthData authParams) {
        try {
            DBApi.init();
            System.out.println(authParams);

            String login = authParams.getLogin();
            String password = authParams.getPassword();

            User user = dbApi.findByLoginAndPwd(login, password);
            if (user == null) {
                user = new User();
                user.setLogin("-1");
            }
            return Response.ok(user).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(User user) {
        try {
            System.out.println(user);
            DBApi.init();
            if (user != null) {
                User userOld = dbApi.findByLogin(user.getLogin());
                if (userOld == null) {
                    user.setRole("USER");
                    dbApi.setObject(user);
                    return Response.ok().build();
                } else {
                    return Response.ok("USER_EXISTS").build();
                }
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
    
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, UserPojo userNew) {
        try {
            DBApi.init();
            if (id != null) {
                User user = (User) dbApi.getObjectByTypeAndId(User.class, id);
                if (user != null && userNew != null) {
                    user.setRole(userNew.getRole());
                    dbApi.setObject(user);
                    return Response.ok().build();
                }
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

}
