package services;

import entity.Tournament;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pojo.TournamentPojo;
import utility.DBApi;

/**
 * @author Админ
 */
@Path("tournaments")
public class TournamentsResource {

    private DBApi dbApi = new DBApi();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        try {
            DBApi.init();
            List<Tournament> list = dbApi.getAllByType(Tournament.class);
            if (list != null && !list.isEmpty()) {
                TournamentPojo[] tournaments = new TournamentPojo[list.size()];
                for (int i = 0; i < tournaments.length; i++) {
                    tournaments[i] = new TournamentPojo(list.get(i));
                    if(tournaments[i].getStatus().toLowerCase().equals("new")){
                        tournaments[i].setStatus("Новый");
                    } else if(tournaments[i].getStatus().toLowerCase().equals("archived")){
                        tournaments[i].setStatus("Заархивирован");
                    } else if(tournaments[i].getStatus().toLowerCase().equals("started")){
                        tournaments[i].setStatus("Начато");
                    } else if(tournaments[i].getStatus().toLowerCase().equals("finished")){
                        tournaments[i].setStatus("Закончен");
                    }
                }
                return Response.ok(tournaments).build();
            }
            return Response.ok(new TournamentPojo[0]).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        try {
            System.out.println(id);
            DBApi.init();
            Tournament tournament = (Tournament) dbApi.getObjectByTypeAndId(Tournament.class, id);
            if(tournament != null && tournament.getStatus().toLowerCase() == "новый" || tournament.getStatus().toLowerCase() == "new"){
                dbApi.delete(tournament);
            }
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(Tournament tournament) {
        try {
            System.out.println(tournament.getName());   
            DBApi.init();
            if (tournament != null) {
                tournament.setStatus("NEW");
                dbApi.setObject(tournament);
                return Response.ok().build();
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, Tournament tournamentNew) {
        try {
            System.out.println(tournamentNew);
            DBApi.init();
            if (id != null) {
                Tournament tournament = (Tournament) dbApi.getObjectByTypeAndId(Tournament.class, id);
                if (tournament != null && tournamentNew != null) {
                    if(tournamentNew != null && tournamentNew.getStatus().toLowerCase().equals("новый") || tournamentNew.getStatus().toLowerCase().equals("new")){
                        tournament.setName(tournamentNew.getName());
                    }
                    tournament.setStatus(tournamentNew.getStatus());
                    dbApi.setObject(tournament);
                    return Response.ok().build();
                }
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
}
