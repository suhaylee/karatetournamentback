/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Participant;
import entity.Games;
import entity.Tournament;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import pojo.GamesPojo;
import pojo.ParticipantPojo;
import pojo.TournamentPojo;
import utility.DBApi;

/**
 * REST Web Service
 *
 * @author Админ
 */
@Path("games")
public class GamesResource {

    private DBApi dbApi = new DBApi();
    
    @GET
    @Path("/{tournamentId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@PathParam("tournamentId") Integer tournamentId) {
        try {
            DBApi.init();
            
            System.out.println(tournamentId);
            
            List<Games> list = dbApi.getAllGamesByTournamentId(tournamentId);
            if (list != null && !list.isEmpty()) {
                GamesPojo[] games = new GamesPojo[list.size()];
                for (int i = 0; i < games.length; i++) {
                    //System.out.println("WinnerId: " + list.get(i).getWinner());
                    games[i] = new GamesPojo(list.get(i));
                    if(list.get(i).getParticipantByParticipant1()!=null){
                        Participant part = (Participant) dbApi.getObjectByTypeAndId(Participant.class, list.get(i).getParticipantByParticipant1().getId());
                        ParticipantPojo parti = new ParticipantPojo(part);
                        games[i].setParticipantByParticipant1(parti);
                    } else{
                        games[i].setParticipantByParticipant1(null);
                    }
                    if(list.get(i).getParticipantByParticipant2()!=null){
                        Participant part = (Participant) dbApi.getObjectByTypeAndId(Participant.class, list.get(i).getParticipantByParticipant2().getId());
                        ParticipantPojo parti = new ParticipantPojo(part);
                        games[i].setParticipantByParticipant2(parti);
                    } else{
                        games[i].setParticipantByParticipant2(null);
                    }
                    //System.out.println("GameId: " + games[i].getParticipantByParticipant1().getId());
                }
                if(games.length>0){
                    Tournament tournament = (Tournament) dbApi.getObjectByTypeAndId(Tournament.class, tournamentId);
                    Participant part = (Participant) dbApi.getObjectByTypeAndId(Participant.class, games[0].getParticipantByParticipant1().getId());
                    ParticipantPojo parti = new ParticipantPojo(part);
                    parti.setClub(tournament.getStatus());
                    games[0].setParticipantByParticipant1(parti);
                }
                return Response.ok(games).build();
            }
            return Response.ok(new GamesPojo[0]).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
    
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, GamesPojo gameNew) {
        try {
            System.out.println(gameNew);
            DBApi.init();
            if (id != null) {
                Games game = (Games) dbApi.getObjectByTypeAndId(Games.class, id);
                if (game != null && gameNew != null && game.getTournament().getStatus().toLowerCase().equals("started")){
                    game.setComment(gameNew.getComment());
                    game.setWinner(gameNew.getWinner());
                    dbApi.setObject(game);
                    return Response.ok().build();
                }
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
    
    @GET
    @Path("/{tournamentId}/getWinners")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWinners(@PathParam("tournamentId") Integer tournamentId) {
        try {
            DBApi.init();
            
            Tournament tournament = (Tournament) dbApi.getObjectByTypeAndId(Tournament.class, tournamentId);
            
            if(tournament.getStatus().toLowerCase().equals("archived") || tournament.getStatus().toLowerCase().equals("finished")){
                List<Games> list = dbApi.getAllGamesByTournamentId(tournamentId);

                int cnt=0;
                if (list != null && !list.isEmpty()) {
                    Games[] games = new Games[list.size()];
                    int[] used = new int[10005];
                    int[] allPartWin = new int[10005];
                    cnt=0;
                    for(int i=0;i<games.length;i++)used[i]=0;
                    for (int i = games.length-1; i >= 0; i--) {
                        games[i] = new Games(list.get(i));
                        if(games[i].getParticipantByParticipant1().getWeight()<10005 && games[i].getParticipantByParticipant1().getSex().toLowerCase().equals("male")){
                            int w=(int)(games[i].getParticipantByParticipant1().getWeight()/3)*3;
                            if(used[w]<4){
                                used[w]+=2;
                                allPartWin[cnt++] = games[i].getParticipantByParticipant1().getId();
                                if(games[i].getParticipantByParticipant2()!=null){
                                    allPartWin[cnt++] = games[i].getParticipantByParticipant2().getId();
                                }
                            }
                        }
                    }
                    for(int i=0;i<games.length;i++)used[i]=0;
                    for (int i = games.length-1; i >= 0; i--) {
                        games[i] = new Games(list.get(i));
                        if(games[i].getParticipantByParticipant1().getWeight()<10005 && games[i].getParticipantByParticipant1().getSex().toLowerCase().equals("female")){
                            int w=(int)(games[i].getParticipantByParticipant1().getWeight()/3)*3;
                            if(used[w]<4){
                                used[w]+=2;
                                allPartWin[cnt++] = games[i].getParticipantByParticipant1().getId();
                                if(games[i].getParticipantByParticipant2()!=null){
                                    allPartWin[cnt++] = games[i].getParticipantByParticipant2().getId();
                                }
                            }
                        }
                    }
                    Participant[] allwinners = new Participant[cnt];
                    System.out.println("Len: " + cnt);
                    for(int i=0;i<cnt;i++){
                        System.out.println("here");
                        Participant parti = (Participant) dbApi.getObjectByTypeAndId(Participant.class, allPartWin[i]);
                        allwinners[i]=new Participant(parti);
                    }
                    return Response.ok(allwinners).build();
                }
                return Response.noContent().build();
            }
            return Response.noContent().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            DBApi.destroy();
        }
    }
    
    @POST
    @Path("/{tournamentId}/startNextTour")
    @Produces(MediaType.APPLICATION_JSON)
    public void startNextTour(@PathParam("tournamentId") Integer tournamentId) {
        try {
            DBApi.init();
            
            Tournament tournament = (Tournament) dbApi.getObjectByTypeAndId(Tournament.class, tournamentId);
            
            if(tournament.getStatus().toLowerCase().equals("started")){
                List<Games> list = dbApi.getAllGamesByTournamentId(tournamentId);

                Participant[] allwinners = new Participant[10005];

                int tour = 1, cnt=0;

                if (list != null && !list.isEmpty()) {
                    Games[] games = new Games[list.size()];
                    for (int i = games.length-1; i >= 0; i--) {
                        games[i] = new Games(list.get(i));
                        if(i!=games.length-1 && games[i+1].getTour()!=games[i].getTour()){
                            tour = games[i+1].getTour()+1;
                            break;
                        }
                        if(games[i].getWinner() == 1){
                            cnt++;
                        } else if(games[i].getWinner() == 2){
                            cnt++;
                        }
                    }
                    allwinners = new Participant[cnt];
                    cnt=0;
                    for (int i = games.length-1; i >= 0; i--) {
                        games[i] = new Games(list.get(i));
                        if(i!=games.length-1 && games[i+1].getTour()!=games[i].getTour()){
                            tour = games[i+1].getTour()+1;
                            break;
                        }
                        if(games[i].getWinner() == 1){
                            System.out.println("Idwka: " + games[i].getParticipantByParticipant1().getId());
                            Participant parti = (Participant) dbApi.getObjectByTypeAndId(Participant.class, games[i].getParticipantByParticipant1().getId());
                            allwinners[cnt++] = new Participant(parti);
                        } else if(games[i].getWinner() == 2){
                            Participant parti = (Participant) dbApi.getObjectByTypeAndId(Participant.class, games[i].getParticipantByParticipant2().getId());
                            allwinners[cnt++] = new Participant(parti);
                        }
                    }
                    if(tour==1){
                        tour=2;
                    }
                } else{
                    List<Participant> partsList = dbApi.getAllParticipantsByTournamentId(tournamentId);
                    if (partsList != null && !partsList.isEmpty()) {
                        allwinners = new Participant[partsList.size()];
                        for (int i = 0; i < allwinners.length; i++) {
                            allwinners[i] = new Participant(partsList.get(i));
                        }
                    }
                }
                /*System.out.println("Len: " + cnt);
                for(int i=0;i<allwinners.length;i++){
                    System.out.println("Part i: " + allwinners[i].getName() + " " + allwinners[i].getWeight());
                }*/
                Arrays.sort(allwinners, (a,b) -> Double.valueOf(a.getWeight()).compareTo(Double.valueOf(b.getWeight())));
                boolean found = false;
                for(int i=0;i<allwinners.length;){
                    Participant part = (Participant) dbApi.getObjectByTypeAndId(Participant.class, allwinners[i].getId());
                    System.out.println("Starts: " + allwinners[i].getName() + " " + part.getWeight());
                    Participant[] participants = new Participant[allwinners.length+1];
                    int j=0,c=i;
                    while(i<allwinners.length){
                        if(Math.round(allwinners[i].getWeight())!=Math.round(allwinners[c].getWeight()) && (Math.round(allwinners[i].getWeight())%3==0 || Math.round(allwinners[c].getWeight())+2<Math.round(allwinners[i].getWeight()))){
                            break;
                        }
                        if(allwinners[i].getSex().toLowerCase().equals("male") || allwinners[i].getSex().toLowerCase().equals("мужской")){
                            participants[j++] = new Participant(allwinners[i]);
                        }
                        i++;
                    }
                    if(j >= 2){
                        found = true;
                        createGame(participants,j, tour);
                    }
                }
                for(int i=0;i<allwinners.length;){
                    Participant part = (Participant) dbApi.getObjectByTypeAndId(Participant.class, allwinners[i].getId());
                    System.out.println("Starts: " + allwinners[i].getName() + " " + part.getWeight());
                    Participant[] participants = new Participant[allwinners.length+1];
                    int j=0,c=i;
                    while(i<allwinners.length){
                        if(Math.round(allwinners[i].getWeight())!=Math.round(allwinners[c].getWeight()) && (Math.round(allwinners[i].getWeight())%3==0 || Math.round(allwinners[c].getWeight())+2<Math.round(allwinners[i].getWeight()))){
                            break;
                        }
                        if(allwinners[i].getSex().toLowerCase().equals("female") || allwinners[i].getSex().toLowerCase().equals("женский")){
                            participants[j++] = new Participant(allwinners[i]);
                        }
                        i++;
                    }
                    if(j >= 2){
                        found = true;
                        createGame(participants,j, tour);
                    }
                }
                if(!found){
                    System.out.println("Finisheeed");
                    tournament.setStatus("FINISHED");
                    dbApi.setObject(tournament);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBApi.destroy();
        }
    }
    
    public void createGame(Participant[] participants,int n, int tour) {
        boolean used[] = new boolean[1005];    
        for(int i=0;i<n;i++){
            if(!used[i]){
                Participant parti = (Participant) dbApi.getObjectByTypeAndId(Participant.class, participants[i].getId());
                used[i]=true;
                boolean f=false;
                System.out.println("hereCreateGame: " + parti.getClub().getId());
                for(int j=0;j<n;j++){
                    System.out.println("hereCreateGamej: " + participants[j].getId());
                    Participant partj = (Participant) dbApi.getObjectByTypeAndId(Participant.class, participants[j].getId());
                    System.out.println("hereCreateGamej: " + partj.getName());
                    if(!used[j] && partj.getClub().getId()!=parti.getClub().getId() && partj.getRating()!=parti.getRating()){
                        Games game = new Games();
                        game.setParticipantByParticipant1(parti);
                        game.setParticipantByParticipant2(partj);
                        game.setTournament(parti.getTournament());
                        game.setTour(tour);
                        used[j]=true;
                        f=true;
                        dbApi.setObject(game);
                        break;
                    }
                }
                if(!f){
                    for(int j=0;j<n;j++){
                        Participant partj = (Participant) dbApi.getObjectByTypeAndId(Participant.class, participants[j].getId());
                        if(!used[j] && partj.getClub().getId()!=parti.getClub().getId()){
                            Games game = new Games();
                            game.setParticipantByParticipant1(participants[i]);
                            game.setParticipantByParticipant2(participants[j]);
                            game.setTournament(parti.getTournament());
                            game.setTour(tour);
                            used[j]=true;
                            f=true;
                            dbApi.setObject(game);
                            break;
                        }
                    }
                }
                if(!f){
                    for(int j=0;j<n;j++){
                        if(!used[j]){
                            Games game = new Games();
                            game.setParticipantByParticipant1(participants[i]);
                            game.setParticipantByParticipant2(participants[j]);
                            game.setTournament(parti.getTournament());
                            game.setTour(tour);
                            used[j]=true;
                            f=true;
                            dbApi.setObject(game);
                            break;
                        }
                    }
                }
                if(!f){
                    Games game = new Games();
                    game.setParticipantByParticipant1(parti);
                    game.setTournament(parti.getTournament());
                    game.setTour(tour);
                    game.setWinner(1);
                    f=true;
                    dbApi.setObject(game);
                }
            }
        }
    }
    
}